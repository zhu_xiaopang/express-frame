/*
 * @Author: Soulmate
 * @Date: 2023-02-17 15:50:12
 * @LastEditTime: 2023-05-09 15:43:14
 * @LastEditors: Soulmate
 * @Description: 
 * @FilePath: \express-frame\template\src\api\system\file.ts
 * 版权声明
 */
import request from "@/utils/request";
import { AxiosPromise } from "axios";
// import { DeptFormData, LogQueryParam, LogPageResult } from '@/types';

/**
 * 删除日志
 *
 * @param ids
 */
export function deleteFile(ids: string) {
    return request({
      url: `/logs/${ids}`,
      method: 'delete',
    });
  }

/**
 * 删除日志
 *
 * @param ids
 */
export function uploadFile(ids: string) {
  return request({
    url: `/logs/${ids}`,
    method: 'delete',
  });
}

